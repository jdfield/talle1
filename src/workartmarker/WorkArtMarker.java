/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workartmarker;

import Controllers.Controller;
import DAO.ShapesDAO;
import Models.Rectangle;
import Models.Shape;
import Models.Square;
import Views.MainWindow;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.awt.Color;

/**
 *
 * @author jdfield
 */
public class WorkArtMarker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.run();

//        ShapesDAO shapes = new ShapesDAO();
//        Rectangle rectangle = new Rectangle(100, 50, 1, 1, Color.yellow);
//        shapes.addRectangle(rectangle);
//        Square square = new Square(200, 150, 100, Color.red);
//        shapes.addSquare(square);
//        
//        Gson json = new GsonBuilder().setPrettyPrinting().create();
//        String serialized = json.toJson(shapes);
//        System.out.println(serialized);
//        ShapesDAO shapes2 = json.fromJson(serialized, ShapesDAO.class);
//        for (Square s : shapes2.getSquares()) {
//            System.out.println(s.getClass());
//            System.out.println(s.getSide());
//        }
//        
//        
//        MainWindow window = new MainWindow(shapes2);
//        window.setPanel();
//        window.setVisible(true);
    }

}
